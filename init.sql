/*
 Navicat Premium Data Transfer

 Source Server         : 本地3306
 Source Server Type    : MySQL
 Source Server Version : 50740 (5.7.40)
 Source Host           : localhost:3306
 Source Schema         : taopu-server

 Target Server Type    : MySQL
 Target Server Version : 50740 (5.7.40)
 File Encoding         : 65001

 Date: 14/11/2024 11:13:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for boss_permission
-- ----------------------------
DROP TABLE IF EXISTS `boss_permission`;
CREATE TABLE `boss_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL COMMENT '权限名称',
  `code` varchar(128) DEFAULT NULL COMMENT '权限编码',
  `authority` varchar(32) NOT NULL COMMENT '权限code',
  `parent_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '父权限编号',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '权限类型：0 菜单，1 按钮',
  `url` varchar(128) DEFAULT NULL COMMENT '资源路径',
  `icon` varchar(32) DEFAULT NULL COMMENT '预留字段',
  `valid` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有效：0 无效，1 有效',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_idx` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=196 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='权限表';

-- ----------------------------
-- Records of boss_permission
-- ----------------------------
BEGIN;
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (1, '概述总览', 'zong_lan', '1', 0, 0, '/manage/home', NULL, 1, '2021-12-28 15:07:25', '2021-12-28 16:06:04');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (2, '灯光管理', 'deng_guang_guan_li', '2', 0, 0, '/manage/light', NULL, 1, '2020-09-14 14:37:45', '2020-12-16 17:48:41');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (3, '驾驶舱权限', 'jia_shi_cang', '3', 0, 1, '', NULL, 1, '2024-08-29 14:37:45', '2024-08-30 16:54:45');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (4, '强电管理', 'qiang_dian_guan_li', '4', 0, 0, '/manage/electronic', NULL, 1, '2020-05-17 07:06:57', '2020-12-16 17:48:41');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (5, '监控管理', 'jian_kong_guan_li', '5', 0, 0, '/manage/monitor', NULL, 1, '2020-05-16 02:17:28', '2020-12-16 17:48:41');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (6, '灯光秀管理', 'deng_guang_xiu_guan_li', '6', 0, 0, '/manage/lightSmc', NULL, 1, '2024-08-15 16:27:36', '2024-08-30 16:56:29');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (7, '系统管理', 'xi_tong_guan_li', '7', 0, 0, '/manage/system', NULL, 1, '2020-05-16 02:17:28', '2020-12-16 17:48:41');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (8, '我的信息', 'wo_de_xin_xi', '7-3', 7, 0, '/manage/user', NULL, 1, '2020-08-27 09:45:44', '2020-12-30 14:51:23');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (9, '角色管理', 'jue_se_guan_li', '7-2', 7, 0, '/manage/auth', NULL, 1, '2020-09-09 14:33:57', '2020-12-30 14:56:23');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (10, '项目管理', 'xiang_mu_guan_li', '8', 0, 0, '/manage/broker', NULL, 1, '2020-12-17 16:02:02', '2020-12-17 16:02:06');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (11, '认证服务管理', 'ren_zhen_fu_wu_guan_li', '11', 0, 0, '/manage/light/lighting_bossManagement', NULL, 1, '2021-04-07 12:52:01', '2021-04-07 12:52:17');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (91, '添加角色', 'tian_jia_jue_se', '7-2-2', 9, 1, '/auth/addRole', NULL, 1, '2020-11-19 12:00:41', '2020-12-17 15:56:21');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (92, '删除角色', 'shan_chu_jue_se', '7-2-3', 9, 1, '/auth/deleteRole', NULL, 1, '2020-11-30 11:40:32', '2020-12-17 15:56:21');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (93, '编辑角色', 'bian_ji_jue_se', '7-2-4', 9, 1, '/auth/updateRole', NULL, 1, '2020-12-17 15:48:58', '2020-12-17 15:56:21');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (101, '创建项目', 'chuang_jian_xiang_mu', '8-1', 10, 1, '/broke/createBroker', NULL, 1, '2020-12-17 16:25:31', '2021-01-08 15:48:21');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (102, '删除项目', 'shan_chu_xiang_mu', '8-2', 10, 1, '/broke/deleteBroker', NULL, 1, '2020-12-17 16:26:45', '2021-01-08 15:48:21');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (103, '编辑项目', 'bian_ji_xiang_mu', '8-3', 10, 1, '/broke/updateBroker', NULL, 1, '2020-12-17 16:28:29', '2021-01-08 15:48:20');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (104, '视频库管理', 'shi_ping_ku_guan_li', '2-1', 2, 0, '/manage/light/videoLibrary', NULL, 1, '2020-12-17 19:02:56', '2020-12-18 11:55:37');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (105, '节点管理', 'jie_dian_guan_li', '2-3', 2, 0, '/manage/light/nodeManagement', NULL, 1, '2020-12-17 19:03:06', '2020-12-18 11:55:36');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (106, '楼宇管理', 'lou_yu_guan_li', '2-2', 2, 0, '/manage/light/buildingManagement', NULL, 1, '2020-12-17 19:03:09', '2020-12-18 11:55:36');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (108, '用户管理', 'yong_hu_guan_li', '7-4', 7, 0, '/manage/userList', NULL, 1, '2020-12-22 13:32:58', '2021-01-06 11:09:09');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (109, '添加用户', 'tian_jia_yong_hu', '7-4-1', 108, 1, '/user/addUser', NULL, 1, '2020-12-22 13:42:09', '2020-12-22 13:42:11');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (110, '编辑用户', 'bian_ji_yong_hu', '7-4-2', 108, 1, '/user/update', NULL, 1, '2020-12-22 13:42:16', '2020-12-22 13:55:10');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (111, '删除用户', 'shan_chu_yong_hu', '7-4-3', 108, 1, '/user/deleteUser', NULL, 1, '2020-12-22 13:55:10', '2020-12-22 13:55:10');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (114, '操作日志', 'cao_zuo_ri_zhi', '7-5', 7, 0, '/manage/operateLog', NULL, 1, '2020-12-30 15:23:53', '2020-12-30 15:23:55');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (115, '节目管理', 'jie_mu_guan_li', '2-4', 2, 0, '/manage/light/programManagement', NULL, 1, '2021-01-04 21:40:00', '2021-01-04 21:47:40');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (117, '视频删除', 'shi_pin_shan_chu', '2-1-7', 104, 1, '', NULL, 1, '2021-01-08 12:07:32', '2021-01-08 12:07:32');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (118, '视频查看', 'shi_pin_cha_kan', '2-1-1', 104, 1, '', NULL, 1, '2021-01-08 12:07:42', '2021-01-08 12:07:42');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (119, '视频添加', 'shi_pin_tian_jia', '2-1-2', 104, 1, '', NULL, 1, '2021-01-08 12:07:42', '2021-01-08 12:07:42');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (120, '视频编辑', 'shi_pin_bian_ji', '2-1-3', 104, 1, '', NULL, 1, '2021-01-08 12:07:42', '2021-01-08 12:07:42');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (121, '视频批量导入', 'shi_pin_pi_liang_dao_ru', '2-1-4', 104, 1, '', NULL, 1, '2021-01-08 12:07:42', '2021-01-08 12:07:42');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (122, '视频批量删除', 'shi_pin_pi_liang_shan_chu', '2-1-5', 104, 1, '', NULL, 1, '2021-01-08 12:07:42', '2021-01-08 12:07:42');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (124, '片段查看', 'pian_duan_cha_kan', '2-1-11', 104, 1, '', NULL, 1, '2021-01-08 13:55:05', '2021-01-08 14:26:31');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (125, '片段添加', 'pian_duan_tian_jia', '2-1-12', 104, 1, '', NULL, 1, '2021-01-08 13:55:05', '2021-01-08 14:26:31');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (126, '片段编辑', 'pian_duan_bian_ji', '2-1-13', 104, 1, '', NULL, 1, '2021-01-08 13:55:05', '2021-01-08 14:26:31');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (127, '片段批量删除', 'pian_duan_pi_liang_shan_chu', '2-1-14', 104, 1, '', NULL, 1, '2021-01-08 13:55:05', '2021-01-08 14:26:31');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (149, '片段删除', 'pian_duan_shan_chu', '2-1-16', 104, 1, '', NULL, 1, '2021-01-08 13:56:22', '2021-01-08 14:26:31');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (150, '楼宇查看', 'lou_yu_cha_kan', '2-2-1', 106, 1, '', NULL, 1, '2021-01-08 14:01:26', '2021-01-08 14:01:26');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (151, '楼宇添加', 'lou_yu_tian_jia', '2-2-2', 106, 1, '', NULL, 1, '2021-01-08 14:01:26', '2021-01-08 14:01:26');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (152, '楼宇编辑', 'lou_yu_bian_ji', '2-2-3', 106, 1, '', NULL, 1, '2021-01-08 14:01:26', '2021-01-08 14:01:26');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (153, '楼宇批量删除', 'lou_yu_pi_liang_shan_chu', '2-2-4', 106, 1, '', NULL, 1, '2021-01-08 14:01:26', '2021-01-11 20:44:01');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (155, '楼宇删除', 'lou_yu_shan_chu', '2-2-6', 106, 1, '', NULL, 1, '2021-01-08 14:01:26', '2021-01-08 14:01:26');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (156, '媒体墙查看', 'mei_ti_qiang_cha_kan', '2-3-1', 105, 1, '', NULL, 1, '2021-01-08 14:05:04', '2021-01-08 14:08:40');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (157, '媒体墙添加', 'mei_ti_qiang_tian_jia', '2-3-2', 105, 1, '', NULL, 1, '2021-01-08 14:05:04', '2021-01-08 14:08:40');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (158, '媒体墙编辑', 'mei_ti_qiang_bian_ji', '2-3-3', 105, 1, '', NULL, 1, '2021-01-08 14:05:04', '2021-01-08 14:08:40');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (159, '媒体墙批量删除', 'mei_ti_qiang_pi_liang_shan_chu', '2-3-4', 105, 1, '', NULL, 1, '2021-01-08 14:05:04', '2021-01-08 14:08:40');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (161, '媒体墙删除', 'mei_ti_qiang_shan_chu', '2-3-6', 105, 1, '', NULL, 1, '2021-01-08 14:05:04', '2021-01-08 14:08:40');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (162, '双色温查看', 'shuang_se_wen_cha_kan', '2-3-11', 105, 1, '', NULL, 1, '2021-01-08 14:08:01', '2021-01-08 14:08:01');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (163, '双色温添加', 'shuang_se_wen_tian_jia', '2-3-12', 105, 1, '', NULL, 1, '2021-01-08 14:08:01', '2021-01-08 14:25:07');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (164, '双色温编辑', 'shuang_se_wen_bian_ji', '2-3-13', 105, 1, '', NULL, 1, '2021-01-08 14:08:01', '2021-01-08 14:25:07');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (165, '双色温批量删除', 'shuang_se_wen_pi_liang_shan_chu', '2-3-14', 105, 1, '', NULL, 1, '2021-01-08 14:08:01', '2021-01-08 14:25:07');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (167, '双色温删除', 'shuang_se_wen_shan_chu', '2-3-16', 105, 1, '', NULL, 1, '2021-01-08 14:08:01', '2021-01-08 14:25:07');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (174, '节目媒体墙查看', 'jie_mu_mei_ti_qiang_cha_kan', '2-4-1', 115, 1, '', NULL, 1, '2021-01-08 14:20:53', '2021-01-08 14:20:53');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (175, '节目媒体墙添加', 'jie_mu_mei_ti_qiang_tian_jia', '2-4-2', 115, 1, '', NULL, 1, '2021-01-08 14:20:53', '2021-01-08 14:20:53');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (176, '节目媒体墙编辑', 'jie_mu_mei_ti_qiang_bian_ji', '2-4-3', 115, 1, '', NULL, 1, '2021-01-08 14:20:53', '2021-01-08 14:20:53');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (177, '节目媒体墙批量删除', 'jie_mu_mei_ti_qiang_pi_liang_shan_chu', '2-4-4', 115, 1, '', NULL, 1, '2021-01-08 14:20:53', '2021-01-08 14:20:53');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (179, '节目媒体墙删除', 'jie_mu_mei_ti_qiang_shan_chu', '2-4-6', 115, 1, '', NULL, 1, '2021-01-08 14:20:53', '2021-01-08 14:20:53');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (180, '节目双色温查看', 'jie_mu_shuang_se_wen_cha_kan', '2-4-11', 115, 1, '', NULL, 1, '2021-01-08 14:22:26', '2021-01-08 14:22:26');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (181, '节目双色温添加', 'jie_mu_shuang_se_wen_tian_jia', '2-4-12', 115, 1, '', NULL, 1, '2021-01-08 14:22:26', '2021-01-08 14:22:26');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (182, '节目双色温编辑', 'jie_mu_shuang_se_wen_bian_ji', '2-4-13', 115, 1, '', NULL, 1, '2021-01-08 14:22:26', '2021-01-08 14:22:26');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (183, '节目双色温批量删除', 'jie_mu_shuang_se_wen_pi_liang_shan_chu', '2-4-14', 115, 1, '', NULL, 1, '2021-01-08 14:22:26', '2021-01-08 14:22:26');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (185, '节目双色温删除', 'jie_mu_shuang_se_wen_shan_chu', '2-4-16', 115, 16, '', NULL, 1, '2021-01-08 14:22:26', '2021-01-08 14:24:05');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (186, '设备管理', 'she_bei_guan_li', '4-1', 4, 0, '/manage/electronic/deviceManagement', NULL, 1, '2021-03-05 15:31:58', '2021-03-05 15:55:10');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (187, '设备地点管理', 'she_bei_di_dian_guan_li', '4-2', 4, 0, '/manage/electronic/devicePlaceManagement', NULL, 1, '2021-03-05 15:32:44', '2021-03-05 15:55:01');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (189, '报警管理', 'bao_jing_guan_li', '4-4', 4, 0, '/manage/electronic/warningManagement', NULL, 1, '2021-03-05 15:33:28', '2021-03-05 15:54:57');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (190, '能耗统计', 'neng_hao_tong_ji', '4-5', 4, 0, '/manage/electronic/energyStatitics', NULL, 1, '2021-03-05 15:34:40', '2021-03-05 15:55:40');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (191, '节点秘钥显示', 'cha_kan_mi_yao', '2-3-5', 105, 1, '', NULL, 1, '2021-08-16 11:56:58', '2021-08-16 15:00:05');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (192, '安全管理', 'an_quan_guan_li', '2-5', 2, 0, '/manage/light/clientManagement', NULL, 1, '2021-09-27 11:22:28', '2021-09-27 11:23:42');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (194, '融合设备管理', 'rong_he_she_bei_guan_li', '6-1', 6, 0, '/manage/lightSmc/integratedDeviceManagement', NULL, 1, '2024-08-15 16:29:34', '2024-08-30 16:56:31');
INSERT INTO `boss_permission` (`id`, `name`, `code`, `authority`, `parent_id`, `type`, `url`, `icon`, `valid`, `gmt_create`, `gmt_modified`) VALUES (195, '灯光秀节目管理', 'deng_guang_xiu_jie_mu_guan_li', '6-2', 6, 0, '/manage/lightSmc/lightShowProgramManagement', NULL, 1, '2024-08-15 16:31:36', '2024-08-30 16:56:34');
COMMIT;

-- ----------------------------
-- Table structure for boss_role
-- ----------------------------
DROP TABLE IF EXISTS `boss_role`;
CREATE TABLE `boss_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL COMMENT '角色名称',
  `code` varchar(200) NOT NULL COMMENT '角色编码',
  `broker_id` bigint(32) NOT NULL DEFAULT '0' COMMENT '项目ID',
  `authority` varchar(32) DEFAULT NULL COMMENT '角色code',
  `valid` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否有效: 0 无效，1 有效',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_idx` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色表';

-- ----------------------------
-- Records of boss_role
-- ----------------------------
BEGIN;
INSERT INTO `boss_role` (`id`, `name`, `code`, `broker_id`, `authority`, `valid`, `gmt_create`, `gmt_modified`) VALUES (1, '超级管理员', '0-chaojiguanliyuan', 0, NULL, 1, '2021-09-17 15:14:09', '2021-09-17 15:14:14');
INSERT INTO `boss_role` (`id`, `name`, `code`, `broker_id`, `authority`, `valid`, `gmt_create`, `gmt_modified`) VALUES (3, '山东', '1-shandong', 1, NULL, 1, '2024-08-30 17:29:27', '2024-09-01 10:42:45');
COMMIT;

-- ----------------------------
-- Table structure for boss_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `boss_role_permission`;
CREATE TABLE `boss_role_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_code` varchar(128) NOT NULL,
  `permission_code` varchar(128) NOT NULL,
  `valid` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否删除：0 未删除 1 已删除',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_role_permission` (`role_code`,`permission_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=523 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色权限表';

-- ----------------------------
-- Records of boss_role_permission
-- ----------------------------
BEGIN;
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (1, '0-chaojiguanliyuan', 'bao_jing_guan_li', 1, '2021-03-05 15:48:04', '2021-03-05 15:48:30');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (2, '0-chaojiguanliyuan', 'bian_ji_jue_se', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (3, '0-chaojiguanliyuan', 'bian_ji_xiang_mu', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (4, '0-chaojiguanliyuan', 'bian_ji_yong_hu', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (5, '0-chaojiguanliyuan', 'cao_zuo_ri_zhi', 1, '2020-12-30 15:20:58', '2020-12-30 15:21:00');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (6, '0-chaojiguanliyuan', 'cha_kan_mi_yao', 1, '2021-08-16 14:42:01', '2021-08-16 14:42:01');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (7, '0-chaojiguanliyuan', 'chuang_jian_xiang_mu', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (8, '0-chaojiguanliyuan', 'deng_guang_guan_li', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (9, '0-chaojiguanliyuan', 'hu_dong_guang_li', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (10, '0-chaojiguanliyuan', 'jian_kong_guan_li', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (11, '0-chaojiguanliyuan', 'jie_dian_guan_li', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (12, '0-chaojiguanliyuan', 'jie_mu_guan_li', 1, '2021-01-04 21:40:53', '2021-01-04 21:40:56');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (13, '0-chaojiguanliyuan', 'jie_mu_mei_ti_qiang_bian_ji', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (14, '0-chaojiguanliyuan', 'jie_mu_mei_ti_qiang_cha_kan', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (15, '0-chaojiguanliyuan', 'jie_mu_mei_ti_qiang_pi_liang_shan_chu', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (16, '0-chaojiguanliyuan', 'jie_mu_mei_ti_qiang_shan_chu', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (17, '0-chaojiguanliyuan', 'jie_mu_mei_ti_qiang_tian_jia', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (18, '0-chaojiguanliyuan', 'jie_mu_shuang_se_wen_bian_ji', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (19, '0-chaojiguanliyuan', 'jie_mu_shuang_se_wen_cha_kan', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (20, '0-chaojiguanliyuan', 'jie_mu_shuang_se_wen_pi_liang_shan_chu', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (21, '0-chaojiguanliyuan', 'jie_mu_shuang_se_wen_shan_chu', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (22, '0-chaojiguanliyuan', 'jie_mu_shuang_se_wen_tian_jia', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (23, '0-chaojiguanliyuan', 'jue_se_guan_li', 1, '2020-12-22 13:32:58', '2020-12-30 14:55:27');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (24, '0-chaojiguanliyuan', 'lou_yu_bian_ji', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (25, '0-chaojiguanliyuan', 'lou_yu_cha_kan', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (27, '0-chaojiguanliyuan', 'lou_yu_pi_liang_shan_chu', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (28, '0-chaojiguanliyuan', 'lou_yu_shan_chu', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (29, '0-chaojiguanliyuan', 'lou_yu_tian_jia', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (30, '0-chaojiguanliyuan', 'mei_ti_qiang_bian_ji', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (31, '0-chaojiguanliyuan', 'mei_ti_qiang_cha_kan', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (32, '0-chaojiguanliyuan', 'mei_ti_qiang_pi_liang_shan_chu', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (33, '0-chaojiguanliyuan', 'mei_ti_qiang_shan_chu', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (34, '0-chaojiguanliyuan', 'mei_ti_qiang_tian_jia', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (35, '0-chaojiguanliyuan', 'neng_hao_tong_ji', 1, '2021-03-05 15:47:51', '2021-03-05 15:47:51');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (36, '0-chaojiguanliyuan', 'pian_duan_bian_ji', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (37, '0-chaojiguanliyuan', 'pian_duan_cha_kan', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (38, '0-chaojiguanliyuan', 'pian_duan_pi_liang_shan_chu', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (39, '0-chaojiguanliyuan', 'pian_duan_shan_chu', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (40, '0-chaojiguanliyuan', 'pian_duan_tian_jia', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (41, '0-chaojiguanliyuan', 'qiang_dian_guan_li', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (42, '0-chaojiguanliyuan', 'shan_chu_jue_se', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (43, '0-chaojiguanliyuan', 'shan_chu_xiang_mu', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (44, '0-chaojiguanliyuan', 'shan_chu_yong_hu', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (45, '0-chaojiguanliyuan', 'she_bei_di_dian_guan_li', 1, '2021-03-05 15:49:34', '2021-03-05 15:49:38');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (46, '0-chaojiguanliyuan', 'she_bei_guan_li', 1, '2021-03-05 15:47:22', '2021-03-05 15:47:22');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (47, '0-chaojiguanliyuan', 'shi_ping_ku_guan_li', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (48, '0-chaojiguanliyuan', 'shi_pin_bian_ji', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (49, '0-chaojiguanliyuan', 'shi_pin_cha_kan', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (50, '0-chaojiguanliyuan', 'shi_pin_pi_liang_dao_ru', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (51, '0-chaojiguanliyuan', 'shi_pin_pi_liang_shan_chu', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (52, '0-chaojiguanliyuan', 'shi_pin_shan_chu', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (53, '0-chaojiguanliyuan', 'shi_pin_tian_jia', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (54, '0-chaojiguanliyuan', 'shou_quan_yong_hu_jue_se', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (55, '0-chaojiguanliyuan', 'shuang_se_wen_bian_ji', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (56, '0-chaojiguanliyuan', 'shuang_se_wen_cha_kan', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (57, '0-chaojiguanliyuan', 'shuang_se_wen_pi_liang_shan_chu', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (58, '0-chaojiguanliyuan', 'shuang_se_wen_shan_chu', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (59, '0-chaojiguanliyuan', 'shuang_se_wen_tian_jia', 1, '2021-01-07 22:12:06', '2021-01-07 22:12:06');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (60, '0-chaojiguanliyuan', 'tian_jia_jue_se', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (61, '0-chaojiguanliyuan', 'tian_jia_yong_hu', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (62, '0-chaojiguanliyuan', 'wo_de_xin_xi', 1, '2020-12-22 13:32:58', '2020-12-30 14:48:12');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (63, '0-chaojiguanliyuan', 'xiang_mu_guan_li', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (64, '0-chaojiguanliyuan', 'xi_tong_guan_li', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (65, '0-chaojiguanliyuan', 'yong_hu_guan_li', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (66, '0-chaojiguanliyuan', 'zhi_neng_jia_ju', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (67, '0-chaojiguanliyuan', 'an_quan_guan_li', 1, '2020-12-22 13:32:58', '2020-12-22 13:32:58');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (68, '0-chaojiguanliyuan', 'zong_lan', 1, '2021-12-28 15:08:30', '2021-12-28 16:06:15');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (69, '0-chaojiguanliyuan', 'deng_guang_xiu_guan_li', 1, '2024-08-15 16:46:17', '2024-08-15 16:46:17');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (70, '0-chaojiguanliyuan', 'rong_he_she_bei_guan_li', 1, '2024-08-15 16:46:49', '2024-08-15 16:46:49');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (71, '0-chaojiguanliyuan', 'deng_guang_xiu_jie_mu_guan_li', 1, '2024-08-15 16:47:06', '2024-08-15 16:47:16');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (72, '0-chaojiguanliyuan', 'jia_shi_cang', 1, '2024-08-15 16:47:06', '2024-08-15 16:47:16');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (468, '1-shandong', 'an_quan_guan_li', 1, '2024-09-01 10:42:45', '2024-09-01 10:42:45');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (469, '1-shandong', 'bao_jing_guan_li', 1, '2024-09-01 10:42:45', '2024-09-01 10:42:45');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (470, '1-shandong', 'bian_ji_jue_se', 1, '2024-09-01 10:42:45', '2024-09-01 10:42:45');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (471, '1-shandong', 'bian_ji_yong_hu', 1, '2024-09-01 10:42:45', '2024-09-01 10:42:45');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (472, '1-shandong', 'cao_zuo_ri_zhi', 1, '2024-09-01 10:42:45', '2024-09-01 10:42:45');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (473, '1-shandong', 'cha_kan_mi_yao', 1, '2024-09-01 10:42:45', '2024-09-01 10:42:45');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (474, '1-shandong', 'deng_guang_guan_li', 1, '2024-09-01 10:42:45', '2024-09-01 10:42:45');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (475, '1-shandong', 'jia_shi_cang', 1, '2024-09-01 10:42:45', '2024-09-01 10:42:45');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (476, '1-shandong', 'jie_dian_guan_li', 1, '2024-09-01 10:42:45', '2024-09-01 10:42:45');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (477, '1-shandong', 'jie_mu_guan_li', 1, '2024-09-01 10:42:45', '2024-09-01 10:42:45');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (478, '1-shandong', 'jie_mu_mei_ti_qiang_bian_ji', 1, '2024-09-01 10:42:45', '2024-09-01 10:42:45');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (479, '1-shandong', 'jie_mu_mei_ti_qiang_cha_kan', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (480, '1-shandong', 'jie_mu_mei_ti_qiang_pi_liang_shan_chu', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (481, '1-shandong', 'jie_mu_mei_ti_qiang_shan_chu', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (482, '1-shandong', 'jie_mu_mei_ti_qiang_tian_jia', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (483, '1-shandong', 'jie_mu_shuang_se_wen_bian_ji', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (484, '1-shandong', 'jie_mu_shuang_se_wen_cha_kan', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (485, '1-shandong', 'jie_mu_shuang_se_wen_pi_liang_shan_chu', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (486, '1-shandong', 'jie_mu_shuang_se_wen_shan_chu', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (487, '1-shandong', 'jie_mu_shuang_se_wen_tian_jia', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (488, '1-shandong', 'jue_se_guan_li', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (489, '1-shandong', 'mei_ti_qiang_bian_ji', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (490, '1-shandong', 'mei_ti_qiang_cha_kan', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (491, '1-shandong', 'mei_ti_qiang_pi_liang_shan_chu', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (492, '1-shandong', 'mei_ti_qiang_shan_chu', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (493, '1-shandong', 'mei_ti_qiang_tian_jia', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (494, '1-shandong', 'neng_hao_tong_ji', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (495, '1-shandong', 'pian_duan_bian_ji', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (496, '1-shandong', 'pian_duan_cha_kan', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (497, '1-shandong', 'pian_duan_pi_liang_shan_chu', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (498, '1-shandong', 'pian_duan_shan_chu', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (499, '1-shandong', 'pian_duan_tian_jia', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (500, '1-shandong', 'qiang_dian_guan_li', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (501, '1-shandong', 'shan_chu_jue_se', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (502, '1-shandong', 'shan_chu_yong_hu', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (503, '1-shandong', 'she_bei_di_dian_guan_li', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (504, '1-shandong', 'she_bei_guan_li', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (505, '1-shandong', 'shi_ping_ku_guan_li', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (506, '1-shandong', 'shi_pin_bian_ji', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (507, '1-shandong', 'shi_pin_cha_kan', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (508, '1-shandong', 'shi_pin_pi_liang_dao_ru', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (509, '1-shandong', 'shi_pin_pi_liang_shan_chu', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (510, '1-shandong', 'shi_pin_shan_chu', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (511, '1-shandong', 'shi_pin_tian_jia', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (512, '1-shandong', 'shuang_se_wen_bian_ji', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (513, '1-shandong', 'shuang_se_wen_cha_kan', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (514, '1-shandong', 'shuang_se_wen_pi_liang_shan_chu', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (515, '1-shandong', 'shuang_se_wen_shan_chu', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (516, '1-shandong', 'shuang_se_wen_tian_jia', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (517, '1-shandong', 'tian_jia_jue_se', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (518, '1-shandong', 'tian_jia_yong_hu', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (519, '1-shandong', 'wo_de_xin_xi', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (520, '1-shandong', 'xi_tong_guan_li', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (521, '1-shandong', 'yong_hu_guan_li', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
INSERT INTO `boss_role_permission` (`id`, `role_code`, `permission_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (522, '1-shandong', 'zong_lan', 1, '2024-09-01 10:42:46', '2024-09-01 10:42:46');
COMMIT;

-- ----------------------------
-- Table structure for boss_user
-- ----------------------------
DROP TABLE IF EXISTS `boss_user`;
CREATE TABLE `boss_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `broker_id` int(11) DEFAULT '0' COMMENT '券商ID',
  `company_name` varchar(64) DEFAULT NULL,
  `invitation_code` varchar(32) DEFAULT NULL COMMENT '邀请码',
  `inviteer_code` varchar(32) DEFAULT NULL COMMENT '上级邀请码',
  `user_name` varchar(32) NOT NULL COMMENT '账户',
  `password` varchar(128) NOT NULL COMMENT '密码',
  `freeze` tinyint(4) DEFAULT '0' COMMENT '是否冻结',
  `email` varchar(32) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(32) DEFAULT NULL COMMENT '手机号',
  `type` int(11) DEFAULT '1' COMMENT '身份类型：1-boss员,2-员工',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `valid` int(1) NOT NULL DEFAULT '1' COMMENT '是否启用 0 未启用 1启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户表';

-- ----------------------------
-- Records of boss_user
-- ----------------------------
BEGIN;
INSERT INTO `boss_user` (`id`, `broker_id`, `company_name`, `invitation_code`, `inviteer_code`, `user_name`, `password`, `freeze`, `email`, `phone`, `type`, `gmt_create`, `gmt_modified`, `valid`) VALUES (1, 0, '杭州勇电照明有限公司', NULL, NULL, 'admin', '39b7e30a607e3764f8bd47baeb5f82ae', 0, '888@qq.com', '888', 1, '2020-07-03 06:35:52', '2021-09-17 15:20:21', 1);
INSERT INTO `boss_user` (`id`, `broker_id`, `company_name`, `invitation_code`, `inviteer_code`, `user_name`, `password`, `freeze`, `email`, `phone`, `type`, `gmt_create`, `gmt_modified`, `valid`) VALUES (2, 1, NULL, 'TYWXNT', NULL, 'cs', '39b7e30a607e3764f8bd47baeb5f82ae', 0, NULL, NULL, 2, '2024-08-30 15:52:27', '2024-08-30 17:30:54', 0);
INSERT INTO `boss_user` (`id`, `broker_id`, `company_name`, `invitation_code`, `inviteer_code`, `user_name`, `password`, `freeze`, `email`, `phone`, `type`, `gmt_create`, `gmt_modified`, `valid`) VALUES (3, 1, '山东日照岚山', 'RJTV43', NULL, 'rz_admin', '715df1980941375aa0585878a42ea7b7', 0, NULL, NULL, 2, '2024-08-30 17:30:49', '2024-09-01 10:42:51', 1);
COMMIT;

-- ----------------------------
-- Table structure for boss_user_role
-- ----------------------------
DROP TABLE IF EXISTS `boss_user_role`;
CREATE TABLE `boss_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `broker_id` bigint(20) NOT NULL COMMENT '券商ID',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_code` varchar(200) NOT NULL COMMENT '角色编码',
  `valid` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有效：1是有效',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_user_role` (`user_id`,`role_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户角色表';

-- ----------------------------
-- Records of boss_user_role
-- ----------------------------
BEGIN;
INSERT INTO `boss_user_role` (`id`, `broker_id`, `user_id`, `role_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (1, 0, 1, '0-chaojiguanliyuan', 1, '2021-09-17 15:11:36', '2021-09-17 15:15:14');
INSERT INTO `boss_user_role` (`id`, `broker_id`, `user_id`, `role_code`, `valid`, `gmt_create`, `gmt_modified`) VALUES (7, 1, 3, '1-shandong', 1, '2024-09-01 10:42:51', '2024-09-01 10:42:51');
COMMIT;

-- ----------------------------
-- Table structure for taopu_module_info
-- ----------------------------
DROP TABLE IF EXISTS `taopu_module_info`;
CREATE TABLE `taopu_module_info` (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `module_type` tinyint(4) DEFAULT NULL COMMENT '板块类型',
  `module_back_url` varchar(1000) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '背景图',
  `module_title` varchar(1000) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '名称',
  `deleted` tinyint(4) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='产业模块信息';

-- ----------------------------
-- Records of taopu_module_info
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for taopu_sub_application_info
-- ----------------------------
DROP TABLE IF EXISTS `taopu_sub_application_info`;
CREATE TABLE `taopu_sub_application_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `sub_module_id` bigint(20) NOT NULL COMMENT '子系统ID',
  `title` tinyint(4) NOT NULL DEFAULT '0' COMMENT '应用名称',
  `iconUrl` varchar(1000) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '图标链接',
  `openUrl` varchar(1000) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '跳转链接',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='子系统中应用信息';

-- ----------------------------
-- Records of taopu_sub_application_info
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for taopu_sub_module_info
-- ----------------------------
DROP TABLE IF EXISTS `taopu_sub_module_info`;
CREATE TABLE `taopu_sub_module_info` (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `main_title` tinyint(4) NOT NULL COMMENT '子系统名称',
  `main_title_back_url` varchar(1000) COLLATE utf8mb4_bin NOT NULL COMMENT '背景图',
  `module_id` bigint(20) NOT NULL COMMENT '模块ID',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='子系统信息';

-- ----------------------------
-- Records of taopu_sub_module_info
-- ----------------------------
BEGIN;
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;


CREATE TABLE `verify_data_registry` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL COMMENT '钱包地址',
  `zkp_code` varchar(255) DEFAULT NULL COMMENT 'zkp序列号',
  `id_num` varchar(255) DEFAULT NULL COMMENT '身份编号',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `zkp_secret_data` text COMMENT 'zkp加密信息',
  `zk_status` int(11) DEFAULT NULL COMMENT '0-待验证，1-验证使用中，2-验证完成，3-已上链',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;