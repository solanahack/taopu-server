//package com.taobao.server.controller;
//
//import cn.hutool.crypto.SecureUtil;
//
//import com.taobao.server.common.api.CommonResult;
//import com.taobao.server.common.jwt.UserLoginToken;
//import com.taobao.server.datasource.entities.UserDO;
//import com.taobao.server.domain.params.UserParams;
//import com.taobao.server.exception.Asserts;
//import com.taobao.server.repositories.UserRepository;
//import com.taobao.server.services.RedisService;
//import io.swagger.annotations.Api;
//import org.springframework.beans.BeanUtils;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//
///**
// * @Authoralex
// * @create2024/11/10 下午1:48
// */
//@Api(description = "登陆相关")
//@RestController
//@RequestMapping(value = "/User")
//public class UserController {
//    @Resource
//    UserRepository userRepository;
//    @Resource
//    RedisService redisService;
//    @PutMapping(value = "/update")
//    @UserLoginToken
//    public CommonResult updatePwd(@RequestBody UserParams.updateParams updateParams, HttpServletRequest request)throws Exception {
//        //1 检测用户名
//        userRepository.checkUserName(updateParams.getId(),updateParams.getUserName());
//        if(!updateParams.getPassword().equals(updateParams.getOldPassword())){
//            Asserts.fail("两次密码不一致");
//        }
//        UserDO userDO=new UserDO();
//        BeanUtils.copyProperties(updateParams,userDO);
//        userDO.setPassword(SecureUtil.md5(updateParams.getPassword()));
//        userRepository.update(userDO);
//
//        return CommonResult.success();
//    }
//}
