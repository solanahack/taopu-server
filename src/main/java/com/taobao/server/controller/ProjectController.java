//package com.taobao.server.controller;
//
//import com.taobao.server.common.api.CommonResult;
//import com.taobao.server.common.jwt.UserLoginToken;
//import com.taobao.server.datasource.entities.PlateDO;
//import com.taobao.server.datasource.entities.ProjectDO;
//import com.taobao.server.repositories.PlateRepository;
//import com.taobao.server.repositories.ProjectRepository;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.web.bind.annotation.*;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//import java.util.List;
//
///**
// * @Authoralex
// * @create2024/11/16 上午10:39
// */
//@Api(description = "项目管理")
//@RestController
//@RequestMapping("/project")
//public class ProjectController {
//    @Resource
//    private ProjectRepository projectRepository;
//
//    @ApiOperation(value="获取项目列表", notes="获取项目列表", produces="application/json")
//    @GetMapping(value = "/list",produces = { "application/json;charset=utf-8" })
//    @UserLoginToken
//    public CommonResult<List<ProjectDO>> getList(
//            @RequestParam(value = "plateId",required = false) Long plateId
//           ) {
//        List<ProjectDO> projectDOS = projectRepository.getList(plateId);
//
//
//        return CommonResult.success(projectDOS);
//    }
//
//    @ApiOperation(value="添加项目", notes="添加项目", produces="application/json")
//    @PostMapping(value = "/add", produces = {"application/json;charset=utf-8" })
//    @UserLoginToken
//    public CommonResult addResource(
//            @RequestBody ProjectDO projectDO, HttpServletRequest request)throws Exception {
//
//        return CommonResult.success(projectRepository.create(projectDO));
//
//
//    }
//    @ApiOperation(value="修改项目", notes="修改项目", produces="application/json")
//    @PutMapping(value = "/update", produces = {"application/json;charset=utf-8" })
//    @UserLoginToken
//    public CommonResult updateResource(
//            @RequestBody ProjectDO projectDO, HttpServletRequest request)throws Exception {
//        projectRepository.update(projectDO);
//        return CommonResult.success();
//
//    }
//}
