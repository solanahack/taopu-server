//package com.taobao.server.controller;
//
//
//import com.taobao.server.common.api.CommonResult;
//import com.taobao.server.common.api.ResultCode;
//import com.taobao.server.services.FileService;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.util.AntPathMatcher;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//import org.springframework.web.multipart.MultipartFile;
//import org.springframework.web.servlet.HandlerMapping;
//
//import javax.annotation.Resource;
//import javax.servlet.ServletOutputStream;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.util.Date;
//import java.util.Objects;
//
///**
// * @author zw
// * @data 2021/8/17 10:16
// */
//@RestController
//@Slf4j
//@RequestMapping("/file")
//public class FileController {
//
//    @Resource
//    private FileService fileService;
//
//
//
//    @ApiOperation("文件上传")
//    @PostMapping(value = "/upload")
//    public CommonResult uploadFile(MultipartFile file, @RequestParam(value = "id",required = false) Long id, @RequestParam(value = "type",required = false) Byte type, @RequestParam(value = "type",required = false) Byte fileType) {
//        if(Objects.isNull(file)) {
//            return CommonResult.failed();
//        }
//        String originalFileName=file.getOriginalFilename();
//        // 上传文件
//        try {
//            String fileUpload = fileService.uploadFile(file);
//                      return CommonResult.success(fileUpload);
//        } catch (Exception e) {
//            log.error("uploadFile method. uploadFile  error, e:", e);
//
//            return CommonResult.failed(ResultCode.FAILED);
//        }
//    }
//}
