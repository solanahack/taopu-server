//package com.taobao.server.controller;
//
//import com.taobao.server.common.api.CommonResult;
//import com.taobao.server.domain.results.HomeResults;
//import com.taobao.server.services.HomeService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.stereotype.Service;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.Resource;
//import java.util.Date;
//import java.util.List;
//
///**
// * @Authoralex
// * @create2024/11/16 上午11:23
// */
//@Api(description = "模块管理")
//@RestController
//@RequestMapping("/home")
//public class HomeController {
//    @Resource
//    HomeService homeService;
//    @ApiOperation(value="演示首页")
//    @GetMapping(value = "/index")
//    public CommonResult<List<HomeResults.plateResult>> getHome() {
//
//        return CommonResult.success(homeService.getHome());
//    }
//}
