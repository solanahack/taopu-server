//package com.taobao.server.controller;
//
//import cn.hutool.core.convert.Convert;
//import cn.hutool.crypto.SecureUtil;
//
//import com.taobao.server.common.api.CommonResult;
//import com.taobao.server.common.jwt.UserLoginToken;
//import com.taobao.server.datasource.entities.UserDO;
//import com.taobao.server.domain.params.AuthControllerParams;
//import com.taobao.server.domain.results.AuthControllerResults;
//import com.taobao.server.repositories.UserRepository;
//import com.taobao.server.services.AuthService;
//import com.taobao.server.services.RedisService;
//import com.taobao.server.services.TokenService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//import javax.validation.Valid;
//
///**
// * @Authoralex
// * @create2024/10/18 上午11:38
// */
//@Api(description = "登陆相关")
//@RestController
//@RequestMapping(value = "/auth")
//public class AuthController {
//    @Resource
//    AuthService authService;
//    @Resource
//    TokenService tokenService;
//    @Resource
//    RedisService redisService;
//    @Resource
//    UserRepository userRepository;
//    @ApiOperation(value="登陆", notes="登陆", produces="application/json")
//    @PostMapping(value = "/login")
//    public CommonResult<AuthControllerResults.loginResult> login(@Valid @RequestBody AuthControllerParams.loginParams loginParams) {
//
//        String username = loginParams.getUserName().trim();
//
//        UserDO userDO = authService.loginByName(username, SecureUtil.md5(loginParams.getPassword()));
//
//
////        if(onWebSocket.checkOnline(worker.getId().toString())){
////            Asserts.fail("一个账号同时只能在一台设备登陆");
////        }
//
//        AuthControllerResults.loginResult result = new AuthControllerResults.loginResult();
//
//        String token = tokenService.makeToken(userDO);
//
//        result.setToken(token);
//        return CommonResult.success(result);
//    }
//
//    @ApiOperation(value="退出登陆", notes="退出登陆")
//    @PostMapping(value = "/logout")
//    @UserLoginToken
//    public CommonResult logout(HttpServletRequest request) {
//
//        return CommonResult.success("OK");
//    }
//
//
//}
