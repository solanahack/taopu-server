//package com.taobao.server.controller;
//
//import com.taobao.server.common.api.CommonResult;
//import com.taobao.server.common.jwt.UserLoginToken;
//import com.taobao.server.datasource.entities.ModuleDO;
//import com.taobao.server.datasource.entities.ProjectDO;
//import com.taobao.server.repositories.ModuleRepository;
//import com.taobao.server.repositories.ProjectRepository;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.web.bind.annotation.*;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//import java.util.List;
//
///**
// * @Authoralex
// * @create2024/11/16 上午10:39
// */
//@Api(description = "模块管理")
//@RestController
//@RequestMapping("/module")
//public class ModuleController {
//    @Resource
//    private ModuleRepository moduleRepository;
//
//    @ApiOperation(value="获取模块列表", notes="获取模块列表", produces="application/json")
//    @GetMapping(value = "/list",produces = { "application/json;charset=utf-8" })
//    @UserLoginToken
//    public CommonResult<List<ModuleDO>> getList(
//            @RequestParam(value = "projectId",required = false) Long projectId
//           ) {
//        List<ModuleDO> moduleDOS = moduleRepository.getList(projectId);
//
//
//        return CommonResult.success(moduleDOS);
//    }
//
//    @ApiOperation(value="添加模块", notes="添加模块", produces="application/json")
//    @PostMapping(value = "/add", produces = {"application/json;charset=utf-8" })
//    @UserLoginToken
//    public CommonResult addResource(
//            @RequestBody ModuleDO moduleDO, HttpServletRequest request)throws Exception {
//
//        return CommonResult.success(moduleRepository.create(moduleDO));
//
//
//    }
//    @ApiOperation(value="修改模块", notes="修改模块", produces="application/json")
//    @PutMapping(value = "/update", produces = {"application/json;charset=utf-8" })
//    @UserLoginToken
//    public CommonResult updateResource(
//            @RequestBody ModuleDO moduleDO, HttpServletRequest request)throws Exception {
//        moduleRepository.update(moduleDO);
//        return CommonResult.success();
//
//    }
//}
