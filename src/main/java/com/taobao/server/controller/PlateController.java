//package com.taobao.server.controller;
//
//import cn.hutool.core.bean.BeanUtil;
//import com.github.pagehelper.PageInfo;
//import com.taobao.server.common.Constants;
//import com.taobao.server.common.api.CommonPage;
//import com.taobao.server.common.api.CommonResult;
//import com.taobao.server.common.jwt.UserLoginToken;
//import com.taobao.server.datasource.entities.PlateDO;
//import com.taobao.server.repositories.PlateRepository;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.web.bind.annotation.*;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//import java.util.List;
//
///**
// * @Authoralex
// * @create2024/11/16 上午10:39
// */
//@Api(description = "板块管理")
//@RestController
//@RequestMapping("/plate")
//public class PlateController {
//    @Resource
//    private PlateRepository plateRepository;
//
//    @ApiOperation(value="获取板块列表", notes="获取板块列表", produces="application/json")
//    @GetMapping(value = "/list",produces = { "application/json;charset=utf-8" })
//    @UserLoginToken
//    public CommonResult<List<PlateDO>> getList(
//           ) {
//        List<PlateDO> plateDOS = plateRepository.getList();
//
//
//        return CommonResult.success(plateDOS);
//    }
//
//    @ApiOperation(value="添加板块", notes="添加板块", produces="application/json")
//    @PostMapping(value = "/add", produces = {"application/json;charset=utf-8" })
//    @UserLoginToken
//    public CommonResult addResource(
//            @RequestBody PlateDO plateDO, HttpServletRequest request)throws Exception {
//
//        return CommonResult.success(plateRepository.create(plateDO));
//
//
//    }
//    @ApiOperation(value="修改板块", notes="修改板块", produces="application/json")
//    @PutMapping(value = "/update", produces = {"application/json;charset=utf-8" })
//    @UserLoginToken
//    public CommonResult updateResource(
//            @RequestBody PlateDO plateDO, HttpServletRequest request)throws Exception {
//        plateRepository.update(plateDO);
//        return CommonResult.success();
//
//    }
//}
