package com.taobao.server.controller;

import com.taobao.server.common.api.CommonResult;
import com.taobao.server.common.jwt.UserLoginToken;
import com.taobao.server.datasource.entities.VerifyDataRegistry;
import com.taobao.server.repositories.VerifyDataRegistryRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @Author alex
 * @create2024/11/16 上午10:39
 */
@Api(description = "项目管理")
@RestController
@RequestMapping("/verifyRegisty")
public class VerifyDataRegistryController {
    @Resource
    private VerifyDataRegistryRepository verifyRegistryRepository;

    @ApiOperation(value="获取验证注册列表", notes="获取验证注册列表", produces="application/json")
    @GetMapping(value = "/list",produces = { "application/json;charset=utf-8" })

    public CommonResult<List<VerifyDataRegistry>> getList(
            @RequestParam(value = "address",required = false) String address
           ) {
        List<VerifyDataRegistry> VerifyDataRegistryS = verifyRegistryRepository.getList(address);

        return CommonResult.success(VerifyDataRegistryS);
    }

    @ApiOperation(value="记录验证", notes="提交验证", produces="application/json")
    @PostMapping(value = "/add", produces = {"application/json;charset=utf-8" })

    public CommonResult addResource(
            @RequestBody VerifyDataRegistry VerifyDataRegistry, HttpServletRequest request)throws Exception {

        return CommonResult.success(verifyRegistryRepository.create(VerifyDataRegistry));


    }
    @ApiOperation(value="结束验证", notes="结束验证", produces="application/json")
    @PutMapping(value = "/end", produces = {"application/json;charset=utf-8" })

    public CommonResult updateResource(
            @RequestBody VerifyDataRegistry verifyDataRegistry, HttpServletRequest request)throws Exception {
        verifyDataRegistry.setEndTime(new Date());
        verifyRegistryRepository.update(verifyDataRegistry);
        return CommonResult.success();

    }
}
