package com.taobao.server.common.api;

public enum ResultCode implements IErrorCode {

    SUCCESS(200, "操作成功"),
    FAILED(500, "操作失败"),
    RESOURCE_NOT_FOUND(404, "资源不存在"),
    UNAUTHORIZED(401, "暂未登录或token已经过期或者用户不存在"),
    FORBIDDEN(403, "没有相关权限"),
    VALIDATE_FAILED(410, "验证失败"),
    WEB_SOCKET_FAILED(501, "ws失败"),

    SMS_ISERROR(10404, "验证码不正确"),

    ERROR_3010001(3010001, "手机号异常"),


    ;


    private long code;
    private String message;

    private ResultCode(long code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public long getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
