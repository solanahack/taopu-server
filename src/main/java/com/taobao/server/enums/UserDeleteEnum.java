package com.taobao.server.enums;

/**
 * @Authoralex
 * @create2024/10/25 下午1:32
 */
public enum UserDeleteEnum {
    TYPE_1((byte)1, "删除"),
    TYPE_0((byte)0, "正常");

    private Byte code;
    private String value;

    public static UserDeleteEnum getEnumByValue(String value)
    {
        for(UserDeleteEnum m : UserDeleteEnum.values())
        {
            if(m.getValue().equals(value)) {
                return m;
            }
        }

        return UserDeleteEnum.TYPE_0;
    }


    public static UserDeleteEnum getEnumByCode(Byte code)
    {
        for(UserDeleteEnum m : UserDeleteEnum.values())
        {
            if(m.getCode().equals(code)){
                return m;
            }
        }

        return UserDeleteEnum.TYPE_0;
    }

    private UserDeleteEnum(byte code, String value) {
        this.code = code;
        this.value = value;
    }

    public Byte getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
