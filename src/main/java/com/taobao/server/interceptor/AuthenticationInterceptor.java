package com.taobao.server.interceptor;

import cn.hutool.core.convert.Convert;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.taobao.server.common.api.ResultCode;
import com.taobao.server.common.jwt.PassToken;
import com.taobao.server.common.jwt.UserLoginToken;
import com.taobao.server.datasource.entities.UserDO;
import com.taobao.server.exception.ApiException;
import com.taobao.server.exception.Asserts;
import com.taobao.server.repositories.UserRepository;
import com.taobao.server.services.AuthService;
import com.taobao.server.services.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;


/**
 * @author jinbin
 * @date 2018-07-08 20:41
 */
public class AuthenticationInterceptor implements HandlerInterceptor {
    @Autowired
    AuthService authService;

    @Resource
    TokenService tokenService;
    @Resource
    UserRepository userRepository;
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object object) throws Exception {
        String token = tokenService.getTokenFromRequest(httpServletRequest);// 从 http 请求头中取出 token
        // 如果不是映射到方法直接通过
        if(!(object instanceof HandlerMethod)){
            return true;
        }
        HandlerMethod handlerMethod=(HandlerMethod)object;
        Method method=handlerMethod.getMethod();
        //检查是否有passtoken注释，有则跳过认证
        if (method.isAnnotationPresent(PassToken.class)) {
            PassToken passToken = method.getAnnotation(PassToken.class);
            if (passToken.required()) {
                return true;
            }
        }
        //检查有没有需要用户权限的注解
        if (method.isAnnotationPresent(UserLoginToken.class)) {
            UserLoginToken userLoginToken = method.getAnnotation(UserLoginToken.class);
            if (userLoginToken.required()) {
                // 执行认证
                if (token == null) {
                    Asserts.fail(ResultCode.UNAUTHORIZED);
                }
                // 获取 token 中的 user id
                String userId;
                try {
                    userId = tokenService.getUserIdFromToken(token);
                } catch (JWTDecodeException j) {
                    throw new ApiException(ResultCode.UNAUTHORIZED);
                }
                UserDO userDO = userRepository.getOne(Convert.toLong(userId),null);
//
//                if (worker == null) {
//                    Asserts.fail(ResultCode.UNAUTHORIZED);
//                }
                // 验证 token
                JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256(userDO.getPassword())).build();

                try {
                    jwtVerifier.verify(token);
                } catch (JWTVerificationException e) {
//                    e.printStackTrace();
                    throw new ApiException(ResultCode.UNAUTHORIZED);
                }
                return true;
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }
    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
