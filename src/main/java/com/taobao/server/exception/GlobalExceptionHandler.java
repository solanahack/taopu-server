package com.taobao.server.exception;

import com.taobao.server.common.api.CommonResult;
import com.taobao.server.common.api.ResultCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.EOFException;

/**
 * 全局异常处理
 * Created by kehh on 2021/8/18.
 */
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ResponseBody
    @ExceptionHandler(value = ApiException.class)
    public CommonResult handle(ApiException e) {
        if (e.getErrorCode() != null) {
            return CommonResult.failed(e.getErrorCode(), e.getMessage());
        }
        return CommonResult.failed(e.getMessage());
    }

    @ExceptionHandler(value = EOFException.class)
    public CommonResult handle(EOFException e) {

        return CommonResult.failed(ResultCode.WEB_SOCKET_FAILED);
    }

    @ResponseBody
    @ExceptionHandler(value = NullPointerException.class)
    public CommonResult handle(HttpServletRequest request, NullPointerException e) {
        e.printStackTrace();
        log.error( "getParameterMap:" + request.getParameterMap() );
        return CommonResult.failed(ResultCode.FAILED, "请联系管理员");
    }

    @ResponseBody
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public CommonResult handleValidException(MethodArgumentNotValidException e) {
        BindingResult bindingResult = e.getBindingResult();
        String message = null;
        if (bindingResult.hasErrors()) {
            FieldError fieldError = bindingResult.getFieldError();
            if (fieldError != null) {
                message = fieldError.getField()+fieldError.getDefaultMessage();
            }
        }
        return CommonResult.validateFailed(message);
    }

    @ResponseBody
    @ExceptionHandler(value = BindException.class)
    public CommonResult handleValidException(BindException e) {
        BindingResult bindingResult = e.getBindingResult();
        String message = null;
        if (bindingResult.hasErrors()) {
            FieldError fieldError = bindingResult.getFieldError();
            if (fieldError != null) {
                message = fieldError.getField()+fieldError.getDefaultMessage();
            }
        }
        return CommonResult.validateFailed(message);
    }

}
