package com.taobao.server.exception;


import com.taobao.server.common.api.IErrorCode;

public class Asserts {

    public static void fail(String message) {
        throw new ApiException(message);
    }

    public static void fail(IErrorCode errorCode, String appendMesage) {
        throw new ApiException(errorCode, appendMesage);
    }

    public static void fail(IErrorCode errorCode) {
        throw new ApiException(errorCode);
    }
}
