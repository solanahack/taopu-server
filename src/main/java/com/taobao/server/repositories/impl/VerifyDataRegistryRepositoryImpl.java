package com.taobao.server.repositories.impl;

import com.github.pagehelper.PageInfo;
import com.taobao.server.datasource.entities.VerifyDataRegistry;
import com.taobao.server.datasource.entities.VerifyDataRegistryExample;
import com.taobao.server.datasource.mappers.VerifyDataRegistryMapper;
import com.taobao.server.exception.Asserts;
import com.taobao.server.repositories.VerifyDataRegistryRepository;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Authoralex
 * @create2024/11/10 下午12:26
 */
@Repository
public class VerifyDataRegistryRepositoryImpl implements VerifyDataRegistryRepository {
    @Resource
    VerifyDataRegistryMapper verifyDataRegistryMapper;
    @Override
    public VerifyDataRegistry getOne(Long id, Boolean isThrow) {
        VerifyDataRegistry VerifyDataRegistry = verifyDataRegistryMapper.selectByPrimaryKey(id);

        if (VerifyDataRegistry == null && isThrow) {
            Asserts.fail("模块");
        }
        return VerifyDataRegistry;
    }

    @Override
    public PageInfo getList(VerifyDataRegistryExample VerifyDataRegistryExample, Integer pageSize, Integer pageNum) {
        return null;
    }

    @Override
    public PageInfo getList(String search, Integer pageSize, Integer pageNum) {
        return null;
    }

    @Override
    public List<VerifyDataRegistry> getList(VerifyDataRegistryExample verifyDataRegistryExample) {
        return verifyDataRegistryMapper.selectByExample(verifyDataRegistryExample);
    }

    @Override
    public Long create(VerifyDataRegistry verifyDataRegistry) {
        return verifyDataRegistryMapper.insertSelective(verifyDataRegistry);
    }

    @Override
    public void delete(Long id) {
        verifyDataRegistryMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void update(VerifyDataRegistry verifyDataRegistry) {
        verifyDataRegistryMapper.updateByPrimaryKeySelective(verifyDataRegistry);
    }

    @Override
    public List<VerifyDataRegistry> getList(String address) {
        VerifyDataRegistryExample verifyDataRegistryExample=new VerifyDataRegistryExample();
        VerifyDataRegistryExample.Criteria criteria=verifyDataRegistryExample.createCriteria();
        if(Objects.isNull(address)) {
            return null;
        }
        criteria.andAddressEqualTo(address);
        return verifyDataRegistryMapper.selectByExample(verifyDataRegistryExample);
    }
}
