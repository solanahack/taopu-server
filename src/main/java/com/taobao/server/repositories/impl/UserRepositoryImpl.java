package com.taobao.server.repositories.impl;

import cn.hutool.core.date.DateUtil;
import com.github.pagehelper.PageInfo;

import com.taobao.server.datasource.entities.UserDO;
import com.taobao.server.datasource.entities.UserDOExample;
import com.taobao.server.datasource.mappers.UserDOMapper;
import com.taobao.server.enums.UserDeleteEnum;
import com.taobao.server.exception.Asserts;
import com.taobao.server.repositories.UserRepository;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Authoralex
 * @create2024/11/10 下午12:26
 */
@Repository
public class UserRepositoryImpl implements UserRepository {
    @Resource
    UserDOMapper userDOMapper;
    @Override
    public UserDO getOne(Long id, Boolean isThrow) {
        UserDO userDO = userDOMapper.selectByPrimaryKey(id);

        if (userDO == null && isThrow) {
            Asserts.fail("用户");
        }
        return userDO;
    }

    @Override
    public PageInfo getList(UserDOExample userDOExample, Integer pageSize, Integer pageNum) {
        return null;
    }

    @Override
    public PageInfo getList(String search, Integer pageSize, Integer pageNum) {
        return null;
    }

    @Override
    public List<UserDO> getList(UserDOExample userDOExample) {
        return userDOMapper.selectByExample(userDOExample);
    }

    @Override
    public Long create(UserDO userDO) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void update(UserDO userDO) {
        userDO.setGmtModified(DateUtil.date());
        userDOMapper.updateByPrimaryKeySelective(userDO);
    }

    @Override
    public void checkUserName(Long id,String userName) {
        UserDOExample userDOExample=new UserDOExample();
        userDOExample.createCriteria().andIdNotEqualTo(id).andUserNameEqualTo(userName).andIsDeleteEqualTo(UserDeleteEnum.TYPE_0.getCode());
        List<UserDO> userDOS=userDOMapper.selectByExample(userDOExample);
        if (userDOS.isEmpty()) {
            Asserts.fail("账户名已存在");
        }
    }
}
