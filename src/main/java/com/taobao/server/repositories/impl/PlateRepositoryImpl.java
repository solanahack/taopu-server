package com.taobao.server.repositories.impl;

import cn.hutool.core.date.DateUtil;
import com.github.pagehelper.PageInfo;
import com.taobao.server.datasource.entities.PlateDO;
import com.taobao.server.datasource.entities.PlateDOExample;
import com.taobao.server.datasource.entities.UserDO;
import com.taobao.server.datasource.entities.UserDOExample;
import com.taobao.server.datasource.mappers.PlateDOMapper;
import com.taobao.server.datasource.mappers.UserDOMapper;
import com.taobao.server.enums.UserDeleteEnum;
import com.taobao.server.exception.Asserts;
import com.taobao.server.repositories.PlateRepository;
import com.taobao.server.repositories.UserRepository;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Authoralex
 * @create2024/11/10 下午12:26
 */
@Repository
public class PlateRepositoryImpl implements PlateRepository {
    @Resource
    PlateDOMapper plateDOMapper;
    @Override
    public PlateDO getOne(Long id, Boolean isThrow) {
        PlateDO plateDO = plateDOMapper.selectByPrimaryKey(id);

        if (plateDO == null && isThrow) {
            Asserts.fail("板块");
        }
        return plateDO;
    }

    @Override
    public PageInfo getList(PlateDOExample plateDOExample, Integer pageSize, Integer pageNum) {
        return null;
    }

    @Override
    public PageInfo getList(String search, Integer pageSize, Integer pageNum) {
        return null;
    }

    @Override
    public List<PlateDO> getList(PlateDOExample plateDOExample) {
        return plateDOMapper.selectByExample(plateDOExample);
    }

    @Override
    public Long create(PlateDO plateDO) {
        plateDO.setCreateTime(DateUtil.date());
        plateDOMapper.insertSelective(plateDO);
        return plateDO.getId();
    }

    @Override
    public void delete(Long id) {
        plateDOMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void update(PlateDO plateDO) {
        plateDO.setUpdateTime(DateUtil.date());
        plateDOMapper.updateByPrimaryKeySelective(plateDO);
    }

    @Override
    public List<PlateDO> getList() {
        PlateDOExample plateDOExample=new PlateDOExample();
        return plateDOMapper.selectByExample(plateDOExample);
    }
}
