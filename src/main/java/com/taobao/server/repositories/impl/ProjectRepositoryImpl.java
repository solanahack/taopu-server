package com.taobao.server.repositories.impl;

import cn.hutool.core.date.DateUtil;
import com.github.pagehelper.PageInfo;
import com.taobao.server.datasource.entities.PlateDOExample;
import com.taobao.server.datasource.entities.ProjectDO;
import com.taobao.server.datasource.entities.ProjectDOExample;
import com.taobao.server.datasource.mappers.ProjectDOMapper;
import com.taobao.server.datasource.mappers.ProjectDOMapper;
import com.taobao.server.enums.UserDeleteEnum;
import com.taobao.server.exception.Asserts;
import com.taobao.server.repositories.ProjectRepository;
import com.taobao.server.repositories.UserRepository;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Authoralex
 * @create2024/11/10 下午12:26
 */
@Repository
public class ProjectRepositoryImpl implements ProjectRepository {
    @Resource
    ProjectDOMapper projectDOMapper;
    @Override
    public ProjectDO getOne(Long id, Boolean isThrow) {
        ProjectDO projectDO = projectDOMapper.selectByPrimaryKey(id);

        if (projectDO == null && isThrow) {
            Asserts.fail("项目");
        }
        return projectDO;
    }

    @Override
    public PageInfo getList(ProjectDOExample projectDOExample, Integer pageSize, Integer pageNum) {
        return null;
    }

    @Override
    public PageInfo getList(String search, Integer pageSize, Integer pageNum) {
        return null;
    }

    @Override
    public List<ProjectDO> getList(ProjectDOExample projectDOExample) {
        return projectDOMapper.selectByExample(projectDOExample);
    }

    @Override
    public Long create(ProjectDO projectDO) {
        projectDO.setCreateTime(DateUtil.date());
        projectDOMapper.insertSelective(projectDO);
        return projectDO.getId();
    }

    @Override
    public void delete(Long id) {
        projectDOMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void update(ProjectDO projectDO) {
        projectDO.setUpdateTime(DateUtil.date());
        projectDOMapper.updateByPrimaryKeySelective(projectDO);
    }

    @Override
    public List<ProjectDO> getList(Long plateId) {
        ProjectDOExample projectDOExample=new ProjectDOExample();
        ProjectDOExample.Criteria criteria=projectDOExample.createCriteria();
        if(plateId!=null){
            criteria.andPlateIdEqualTo(plateId);
        }
        return projectDOMapper.selectByExample(projectDOExample);
    }
}
