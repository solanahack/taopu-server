package com.taobao.server.repositories.impl;

import cn.hutool.core.date.DateUtil;
import com.github.pagehelper.PageInfo;
import com.taobao.server.datasource.entities.ModuleDO;
import com.taobao.server.datasource.entities.ModuleDOExample;
import com.taobao.server.datasource.entities.ProjectDOExample;
import com.taobao.server.datasource.mappers.ModuleDOMapper;
import com.taobao.server.datasource.mappers.ModuleDOMapper;
import com.taobao.server.enums.UserDeleteEnum;
import com.taobao.server.exception.Asserts;
import com.taobao.server.repositories.ModuleRepository;
import com.taobao.server.repositories.UserRepository;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Authoralex
 * @create2024/11/10 下午12:26
 */
@Repository
public class ModuleRepositoryImpl implements ModuleRepository {
    @Resource
    ModuleDOMapper moduleDOMapper;
    @Override
    public ModuleDO getOne(Long id, Boolean isThrow) {
        ModuleDO ModuleDO = moduleDOMapper.selectByPrimaryKey(id);

        if (ModuleDO == null && isThrow) {
            Asserts.fail("模块");
        }
        return ModuleDO;
    }

    @Override
    public PageInfo getList(ModuleDOExample moduleDOExample, Integer pageSize, Integer pageNum) {
        return null;
    }

    @Override
    public PageInfo getList(String search, Integer pageSize, Integer pageNum) {
        return null;
    }

    @Override
    public List<ModuleDO> getList(ModuleDOExample moduleDOExample) {
        return moduleDOMapper.selectByExample(moduleDOExample);
    }

    @Override
    public Long create(ModuleDO moduleDO) {
        moduleDO.setCreateTime(DateUtil.date());
        moduleDOMapper.insertSelective(moduleDO);
        return moduleDO.getId();
    }

    @Override
    public void delete(Long id) {
        moduleDOMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void update(ModuleDO moduleDO) {
        moduleDO.setUpdateTime(DateUtil.date());
        moduleDOMapper.updateByPrimaryKeySelective(moduleDO);
    }

    @Override
    public List<ModuleDO> getList(Long projectId) {
        ModuleDOExample moduleDOExample=new ModuleDOExample();
        ModuleDOExample.Criteria criteria=moduleDOExample.createCriteria();
        if(projectId!=null){
            criteria.andProjectIdEqualTo(projectId);
        }
        return moduleDOMapper.selectByExample(moduleDOExample);
    }
}
