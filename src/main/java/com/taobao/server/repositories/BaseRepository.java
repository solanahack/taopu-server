package com.taobao.server.repositories;

import com.github.pagehelper.PageInfo;

import java.util.List;

public interface BaseRepository<T,E> {

//    T getOne(E e);
    T getOne(Long id, Boolean isThrow);

    PageInfo getList(E e, Integer pageSize, Integer pageNum);

    PageInfo getList(String search, Integer pageSize, Integer pageNum);

    List<T> getList(E e);

    Long create(T t);

    void delete(Long id);

    void update(T t);
}
