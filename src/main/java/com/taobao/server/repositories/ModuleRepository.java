package com.taobao.server.repositories;


import com.taobao.server.datasource.entities.*;

import java.util.List;

/**
 * @Authoralex
 * @create2024/11/10 下午12:26
 */
public interface ModuleRepository extends BaseRepository<ModuleDO, ModuleDOExample> {
    List<ModuleDO> getList(Long projectId);

}
