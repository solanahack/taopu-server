package com.taobao.server.repositories;


import com.taobao.server.datasource.entities.*;

import java.util.List;

/**
 * @Authoralex
 * @create2024/11/10 下午12:26
 */
public interface ProjectRepository extends BaseRepository<ProjectDO, ProjectDOExample> {
    List<ProjectDO> getList(Long plateId);
}
