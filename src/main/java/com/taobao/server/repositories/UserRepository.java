package com.taobao.server.repositories;


import com.taobao.server.datasource.entities.UserDO;
import com.taobao.server.datasource.entities.UserDOExample;

/**
 * @Authoralex
 * @create2024/11/10 下午12:26
 */
public interface UserRepository extends BaseRepository<UserDO, UserDOExample> {
    void checkUserName(Long id,String userName);
}
