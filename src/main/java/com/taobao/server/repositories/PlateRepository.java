package com.taobao.server.repositories;


import com.taobao.server.datasource.entities.PlateDO;
import com.taobao.server.datasource.entities.PlateDOExample;
import com.taobao.server.datasource.entities.UserDO;
import com.taobao.server.datasource.entities.UserDOExample;

import java.util.List;

/**
 * @Authoralex
 * @create2024/11/10 下午12:26
 */
public interface PlateRepository extends BaseRepository<PlateDO, PlateDOExample> {
       List<PlateDO> getList();
}
