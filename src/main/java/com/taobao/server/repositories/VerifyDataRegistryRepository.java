package com.taobao.server.repositories;


import com.taobao.server.datasource.entities.*;

import java.util.List;

/**
 * @Authoralex
 * @create2024/11/10 下午12:26
 */
public interface VerifyDataRegistryRepository extends BaseRepository<VerifyDataRegistry, VerifyDataRegistryExample> {
    List<VerifyDataRegistry> getList(String  address);
}
