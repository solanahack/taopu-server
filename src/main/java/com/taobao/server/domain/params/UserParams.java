package com.taobao.server.domain.params;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Authoralex
 * @create2024/10/18 下午2:33
 */
public class UserParams {
    @Data
    public static class updateParams {
        @NotNull(message = "id必填")
        @ApiModelProperty("id")
        private Long id;
        @NotNull(message = "用户名")
        @ApiModelProperty("用户名")
        private String userName;

        @NotNull(message = "新密码")
        @ApiModelProperty("新密码")
        private String password;

        @NotNull(message = "旧密码")
        @ApiModelProperty("旧密码")
        private String oldPassword;
    }
}
