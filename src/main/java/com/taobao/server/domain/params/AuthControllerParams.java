package com.taobao.server.domain.params;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

public class AuthControllerParams {

    @Getter
    @Setter
    public static class loginParams {

        @NotNull(message = "用户名")
        @ApiModelProperty("用户名")
        private String userName;

        @NotNull(message = "密码")
        @ApiModelProperty("密码")
        private String password;
    }


}
