package com.taobao.server.domain.results;

import com.taobao.server.datasource.entities.ModuleDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * @Authoralex
 * @create2024/10/19 上午10:00
 */

public class HomeResults {
   @Data
    public static class plateResult {

        Long id;

        String title;

        String backUrl;

        Date createTime;

        Date updateTime;

        List<projectResult> projectList;

    }
     @Data
    public static class projectResult {
        private Long id;

        private Long plateId;

        private String title;

        private String backUrl;

        private Date createTime;

        private Date updateTime;

        List<ModuleDO> moduleDOList;
    }
}
