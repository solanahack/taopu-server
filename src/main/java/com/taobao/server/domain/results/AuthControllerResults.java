package com.taobao.server.domain.results;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.List;

public class AuthControllerResults {

    @Setter
    @Getter
    public static class loginResult {

        private String token;



    }


    @Setter
    @Getter
    public static class receivingStatisticsResult {

        private receivingStatisticsResultInfo allReceive;

        private receivingStatisticsResultInfo handingReceive;

        private receivingStatisticsResultInfo endReceive;

    }
    @Setter
    @Getter
    public static class receivingStatisticsResultInfo {

        private Integer count=0;
        private HashMap<String, List<Long>> detail;

    }
}
