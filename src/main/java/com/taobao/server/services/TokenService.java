package com.taobao.server.services;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.taobao.server.datasource.entities.UserDO;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;


/**
 * @author kehh
 * @date 2020-08-19
 */
@Service("TokenService")
public class TokenService {

    public String makeToken(UserDO userDO) {
        String token="";
        token= JWT.create().withAudience(userDO.getId().toString())
                .withExpiresAt(new Date(System.currentTimeMillis() + 12 * 60 * 60 * 1000))// 设置过期时间12小时
                .withIssuedAt(new Date(System.currentTimeMillis()))  // 设置当前时间
                .sign(Algorithm.HMAC256(userDO.getPassword()));// 以 password 作为 token 的密钥
        return token;
    }



    public String getTokenFromRequest(HttpServletRequest httpServletRequest) {
        return httpServletRequest.getHeader("Authorization");// 从 http 请求头中取出 token

    }

    public String getUserIdFromToken(HttpServletRequest httpServletRequest) {
        String token = getTokenFromRequest(httpServletRequest);
        return  getUserIdFromToken(token);
    }

    public String getUserIdFromToken(String token) {
        return  JWT.decode(token).getAudience().get(0);
    }





}
