package com.taobao.server.services;

//import com.taobao.server.controller.PlateController;
import com.taobao.server.datasource.entities.ModuleDO;
import com.taobao.server.datasource.entities.PlateDO;
import com.taobao.server.datasource.entities.ProjectDO;
import com.taobao.server.domain.results.HomeResults;
import com.taobao.server.repositories.ModuleRepository;
import com.taobao.server.repositories.PlateRepository;
import com.taobao.server.repositories.ProjectRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import sun.security.pkcs11.Secmod;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @Authoralex
 * @create2024/11/16 上午11:24
 */
@Service
public class HomeService {
    @Resource
    PlateRepository plateRepository;
    @Resource
    ProjectRepository projectRepository;
    @Resource
    ModuleRepository moduleRepository;
    public List<HomeResults.plateResult> getHome(){
        List<HomeResults.plateResult> res=new ArrayList<>();
        List<PlateDO> plateDOS=plateRepository.getList();
        for(PlateDO plateDO:plateDOS){
            HomeResults.plateResult r=new HomeResults.plateResult();
            BeanUtils.copyProperties(plateDO,r);
            List<ProjectDO> projectDOS=projectRepository.getList(plateDO.getId());
            List<HomeResults.projectResult> projectResults=new ArrayList<>();
            if(!projectDOS.isEmpty()){
                for(ProjectDO projectDO:projectDOS){
                    HomeResults.projectResult projectResult=new HomeResults.projectResult();
                    BeanUtils.copyProperties(projectDO,projectResult);
                    List<ModuleDO> moduleDOS=moduleRepository.getList(projectDO.getId());
                    projectResult.setModuleDOList(moduleDOS);
                    projectResults.add(projectResult);
                }

            }
            r.setProjectList(projectResults);
            res.add(r);
        }
        return res;
    }
}
