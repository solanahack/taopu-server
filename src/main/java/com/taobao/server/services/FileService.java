package com.taobao.server.services;


import com.taobao.server.utils.OssUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

/**
 * @Authoralex
 * @create2024/4/12 上午9:28
 */
@Slf4j
@Component
public class FileService {
    @Resource
    private OssUtil ossUtil;
    @Value("${upload.file-location:oss}")
    private String fileLocationType;
    public String  uploadFile(MultipartFile multipartFile) throws Exception {
        if(fileLocationType.equals("oss")){
            // 0-本地服务 1-oss
            return uploadFileToOss(multipartFile);
        }
        return uploadFileToLocal(multipartFile);
    }
    public String  uploadFileToOss(MultipartFile multipartFile) throws Exception {
        Map<String, String> upload = ossUtil.upload(multipartFile);
        return upload.get("loadUrl");
    }

    public String  uploadFileToLocal(MultipartFile file) throws Exception {
        //获取当前时间戳秒值
        //文件名称
        String filePath="/data/heng_tao/file";
        File filep=new File(filePath);
        if(!filep.exists()){
            filep.mkdirs();
        }
        FileOutputStream out=null;
        try{
            String lastFilePath=filePath+file.getOriginalFilename();
            out= new FileOutputStream(lastFilePath);
            out.write(file.getBytes());
            return lastFilePath;
        }catch (Exception e){
            log.error("pac is error! ,Exception:{}",e);
            e.printStackTrace();
        }finally {
            if (out != null) {
                try {
                    out.flush();
                } catch (IOException e) {
                    log.error("uploadFileToLocal is error! ,Exception:{}",e);
                    e.printStackTrace();
                }
                try {
                    out.close();
                } catch (IOException e) {
                    log.error("uploadFileToLocal is error! ,Exception:{}",e);
                    e.printStackTrace();
                }
            }
        }
        throw new Exception("uploadFileToLocal is error");
    }
}
