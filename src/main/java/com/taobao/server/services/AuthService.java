package com.taobao.server.services;


import com.taobao.server.datasource.entities.UserDO;
import com.taobao.server.datasource.entities.UserDOExample;
import com.taobao.server.exception.Asserts;
import com.taobao.server.repositories.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AuthService {
    @Resource
    public UserRepository userRepository;

    public UserDO loginByName(String username, String password) {

        UserDOExample userDOExample=new UserDOExample();
        userDOExample.createCriteria().andUserNameEqualTo(username);

        List<UserDO> userDOS=userRepository.getList(userDOExample);
        if (CollectionUtils.isEmpty(userDOS)) {
            Asserts.fail( "用户不存在");
        }
        if (!userDOS.get(0).getPassword().equals(password)) {
            Asserts.fail("密码错误 ");
        }
        return userDOS.get(0);

    }

}
