package com.taobao.server.datasource.mappers;

import com.taobao.server.datasource.entities.ProjectDO;
import com.taobao.server.datasource.entities.ProjectDOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ProjectDOMapper {
    long countByExample(ProjectDOExample example);

    int deleteByExample(ProjectDOExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ProjectDO record);

    int insertSelective(ProjectDO record);

    List<ProjectDO> selectByExample(ProjectDOExample example);

    ProjectDO selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ProjectDO record, @Param("example") ProjectDOExample example);

    int updateByExample(@Param("record") ProjectDO record, @Param("example") ProjectDOExample example);

    int updateByPrimaryKeySelective(ProjectDO record);

    int updateByPrimaryKey(ProjectDO record);
}