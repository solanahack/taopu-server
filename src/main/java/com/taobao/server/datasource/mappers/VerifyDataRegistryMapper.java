package com.taobao.server.datasource.mappers;

import com.taobao.server.datasource.entities.VerifyDataRegistry;
import com.taobao.server.datasource.entities.VerifyDataRegistryExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface VerifyDataRegistryMapper {
    long countByExample(VerifyDataRegistryExample example);

    int deleteByExample(VerifyDataRegistryExample example);

    int deleteByPrimaryKey(Long id);

    Long insert(VerifyDataRegistry record);

    Long insertSelective(VerifyDataRegistry record);

    List<VerifyDataRegistry> selectByExampleWithBLOBs(VerifyDataRegistryExample example);

    List<VerifyDataRegistry> selectByExample(VerifyDataRegistryExample example);

    VerifyDataRegistry selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") VerifyDataRegistry record, @Param("example") VerifyDataRegistryExample example);

    int updateByExampleWithBLOBs(@Param("record") VerifyDataRegistry record, @Param("example") VerifyDataRegistryExample example);

    int updateByExample(@Param("record") VerifyDataRegistry record, @Param("example") VerifyDataRegistryExample example);

    int updateByPrimaryKeySelective(VerifyDataRegistry record);

    int updateByPrimaryKeyWithBLOBs(VerifyDataRegistry record);

    int updateByPrimaryKey(VerifyDataRegistry record);
}