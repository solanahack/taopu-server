package com.taobao.server.datasource.mappers;

import com.taobao.server.datasource.entities.PlateDO;
import com.taobao.server.datasource.entities.PlateDOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PlateDOMapper {
    long countByExample(PlateDOExample example);

    int deleteByExample(PlateDOExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PlateDO record);

    int insertSelective(PlateDO record);

    List<PlateDO> selectByExample(PlateDOExample example);

    PlateDO selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PlateDO record, @Param("example") PlateDOExample example);

    int updateByExample(@Param("record") PlateDO record, @Param("example") PlateDOExample example);

    int updateByPrimaryKeySelective(PlateDO record);

    int updateByPrimaryKey(PlateDO record);
}