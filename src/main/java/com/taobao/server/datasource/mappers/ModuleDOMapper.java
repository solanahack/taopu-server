package com.taobao.server.datasource.mappers;

import com.taobao.server.datasource.entities.ModuleDO;
import com.taobao.server.datasource.entities.ModuleDOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ModuleDOMapper {
    long countByExample(ModuleDOExample example);

    int deleteByExample(ModuleDOExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ModuleDO record);

    int insertSelective(ModuleDO record);

    List<ModuleDO> selectByExample(ModuleDOExample example);

    ModuleDO selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ModuleDO record, @Param("example") ModuleDOExample example);

    int updateByExample(@Param("record") ModuleDO record, @Param("example") ModuleDOExample example);

    int updateByPrimaryKeySelective(ModuleDO record);

    int updateByPrimaryKey(ModuleDO record);
}