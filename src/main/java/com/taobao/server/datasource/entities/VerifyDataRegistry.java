package com.taobao.server.datasource.entities;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
@Data
public class VerifyDataRegistry {

    @ApiModelProperty(value = "主键Id")
    private Long id;

    @ApiModelProperty(value = "钱包地址")
    private String address;

    @ApiModelProperty(value = "VDR编号")
    private String zkpCode;
    @ApiModelProperty(value = "唯一身份注册")

    private String idNum;
    @ApiModelProperty(value = "开始时间")

    private Date startTime;
    @ApiModelProperty(value = "结束时间")

    private Date endTime;
    @ApiModelProperty(value = "0-凭据待验证，1-凭据使用中，2-凭据使用完成待上链，3-已发送上链，4-mint完成")

    private Integer zkStatus;
    @ApiModelProperty(value = "zk凭据内容")

    private String zkpSecretData;

}