package com.taobao.server;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.FileInputStream;
import java.util.Map;
import java.util.Properties;

@SpringBootApplication
@MapperScan("com.taobao.server.datasource.mappers")
@ServletComponentScan
@EnableScheduling
public class TaopuApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication sa = new SpringApplication(TaopuApplication.class);

//        sa.addListeners(new SetWorkersOffline());

		sa.run(args);	}


}
